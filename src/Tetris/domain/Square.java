/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.domain;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Varpunen
 */
public class Square {

    private ArrayList<Square> allPieces;
    private int areaWidth;
    private int areaHeight;
    private int x;
    private int y;
    private Color color;

    public Square(int areaWidth, int areaHeight, int x, int y, Color color, ArrayList<Square> pieces) {
        this.areaWidth = areaWidth;
        this.areaHeight = areaHeight;
        this.color = color;
        this.x = x;
        this.y = y;
        this.allPieces = pieces;
    }

    private Square(int areaWidth, int areaHeight, int x, int y, ArrayList<Square> pieces) {
        this.areaWidth = areaWidth;
        this.areaHeight = areaHeight;
        this.x = x;
        this.y = y;
        this.allPieces = pieces;
    }

    protected ArrayList<Square> getAllPieces() {
        return allPieces;
    }

    public boolean dropOne() {
        /*boolean answer = (hasSpaceToMoveOne(0, 1));
        if (answer == true) {*/
        y++;
        return true;
        /* }
        return answer;*/
    }

    public void dropDown() {
        while (hasSpaceToMoveOne(0, 1)) {
            dropOne();
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean hasSpaceToMoveOne(int dx, int dy) {
        Square potential = new Square(areaWidth, areaHeight, x + dx, y + dy, allPieces);
        if (potential.getY() >= areaHeight || potential.getX() >= areaWidth || potential.getX() < 0 || potential.getY() < 0) {
            return false;
        }
        for (Square piece : allPieces) {
            if (potential.collides(piece)) {
                return false;
            }
        }
        return true;
    }

    public boolean hasSpaceToMoveTo(int x, int y) {
        for (Square other : allPieces) {
            if (this.collides(other)) {
                return false;
            }
        }
        return true;
    }

    public boolean collides(Square other) {
        if (this.x == other.getX() && this.y == other.getY()) {
            return true;
        }
        return false;
    }

    public Color getColor() {
        return color;
    }

    public void moveLeft() {
        // if (hasSpaceToMoveOne(-1, 0)) {
        x--;
        // }
    }

    public void moveRight() {
        //  if (hasSpaceToMoveOne(1, 0)) {
        x++;
        //  }
    }

    public void turn() {
        // if (hasSpaceToMoveOne(0, -1)) {
        y--;
        // }
    }

}
