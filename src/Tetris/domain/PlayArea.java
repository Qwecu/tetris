/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.domain;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Color;
import java.awt.event.KeyEvent;

/**
 *
 * @author Varpunen
 */
public class PlayArea {

    private ArrayList<Square> allPieces;
    private int width;
    private int height;
    private static Random random;
    private Tetromino movingPiece;
    private boolean pieceHasHitBottom;
    private boolean gameEnded;
    private int clearedRows;

    static {
        random = new Random();
    }

    public PlayArea(int w, int h) {
        width = w;
        height = h;
        allPieces = new ArrayList<>();
        pieceHasHitBottom = true;
        gameEnded = false;
        movingPiece = addTetromino();
        clearedRows = 0;

    }

    public void gameElapse() {
        pieceHasHitBottom = !movingPiece.dropOne();
        if (pieceHasHitBottom) {
            addMovingPieceToAllSquares();
            clearedRows += (clearCompletedRows());
            addNewMovingPiece();
        }
    }

    private void addNewMovingPiece() {

        Tetromino t = addTetromino();
        System.out.println("Tetramino: " + t);
        if (t == null) {
            gameEnded = true;
            return;
        }
        pieceHasHitBottom = false;
        movingPiece = t;
    }

    public Tetromino getMovingPiece() {
        return movingPiece;
    }

    public boolean gameEnded() {
        return gameEnded;
    }
    
    public int getClearedRows(){
        return clearedRows;
    }

    public ArrayList<Square> getVisiblePieces() {
        ArrayList<Square> pieces = new ArrayList<>();
        for (Square allPiece : allPieces) {
            pieces.add(allPiece);
        }
        if (movingPiece != null) {
            for (Square piece : movingPiece.getPieces()) {
                pieces.add(piece);
            }
        }
        return pieces;
    }

    public Tetromino addTetromino() {
        Tetromino tetramino = Tetromino.newTetromino(width, height, new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)), allPieces);
        boolean collides = false;
        for (Square allPiece : allPieces) {
            if (tetramino.collides(allPiece)) {
                collides = true;
            }
        }
        if (!collides) {
            movingPiece = tetramino;
            return tetramino;
        }
        return null;
    }

    public void processKeyPress(KeyEvent ke) {
        int kc = ke.getKeyCode();
        switch (kc) {
            case KeyEvent.VK_LEFT:
                movingPiece.moveLeft();
                break;
            case KeyEvent.VK_UP:
                movingPiece.turn();
                break;
            case KeyEvent.VK_RIGHT:
                movingPiece.moveRight();
                break;
            case KeyEvent.VK_DOWN:
                movingPiece.dropOne();
                break;
            case KeyEvent.VK_SPACE:
                movingPiece.dropDown();
                pieceHasHitBottom = true;
                break;
            default:
                break;
        }
    }

    private int clearCompletedRows() {
        int clearedRowsThisTurn = 0;
        ArrayList<ArrayList<Square>> squaresSortedByRow = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            squaresSortedByRow.add(new ArrayList<Square>());

        }
        for (Square piece : allPieces) {
            squaresSortedByRow.get(piece.getY()).add(piece);
        }
        for (int i = 0; i < squaresSortedByRow.size(); i++) {
            ArrayList<Square> list = squaresSortedByRow.get(i);
            if (list.size() == width) {
                for (Square square : list) {
                    allPieces.remove(square);
                }
                for (int j = i-1; j >=0; j--) {
                    for (Square square : squaresSortedByRow.get(j)) {
                        square.dropOne();
                    }         
                }
                clearedRowsThisTurn++;
            }
        }
        return clearedRowsThisTurn;
    }

    private void addMovingPieceToAllSquares() {
        for (Square piece : movingPiece.getPieces()) {
            allPieces.add(piece);
        }
    }
}
