/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.UI;

import Tetris.domain.*;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import javax.swing.WindowConstants;

/**
 *
 * @author Varpunen
 */
public class Tetris implements Runnable {

    private JFrame frame;
    private PlayAreaGraphics playAreaGraphics;
    private int screenWidth;
    private int screenHeight;
    private int areaWidth;
    private int areaHeight;
    private PlayArea playArea;
    private Timer timer;
    private boolean gameEnded;
    int timerDelay;

    public Tetris() {
        gameEnded = false;
        areaWidth = 10;
        areaHeight = 22;
        screenWidth = 400;
        screenHeight = areaHeight / areaWidth * screenWidth; //keeps the pieces square-shaped
        timerDelay = 1000;

    }

    @Override
    public void run() {
        frame = new JFrame("Tetris");

        frame.setPreferredSize(new Dimension(screenWidth + 20, screenHeight + 40));

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        playArea = new PlayArea(areaWidth, areaHeight);

        createComponents(frame.getContentPane());

        frame.pack();
        frame.setVisible(true);

        ActionListener taskPerformer = (ActionEvent evt) -> {
            if (gameEnded == false) {
                gameElapse();
            } else{
                timer.stop();
                JOptionPane.showMessageDialog(frame, ("You cleared " + playArea.getClearedRows()) + " rows!", "Game over!", JOptionPane.PLAIN_MESSAGE);         
            }
        };
        timer = new Timer(timerDelay, taskPerformer);
        timer.start();

        KeyListener keyListener = new KeyListener() {

            @Override
            public void keyTyped(KeyEvent ke) {

            }

            @Override
            public void keyPressed(KeyEvent ke) {
                playArea.processKeyPress(ke);
                updateGraphics();
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        };
        frame.addKeyListener(keyListener);
    }

    public void createComponents(Container container) {
        playAreaGraphics = new PlayAreaGraphics(screenWidth, screenHeight, areaWidth, areaHeight);
        container.add(playAreaGraphics);
    }

    private void gameElapse() {
        playArea.gameElapse();
        updateGraphics();
        timerDelay--;
        timer.setDelay(timerDelay);
        if (playArea.gameEnded() == true) {
            this.gameEnded = true;
        }
    }

    public JFrame getFrame() {
        return frame;
    }

    public void updateGraphics() {
        playAreaGraphics.setPieces(playArea.getVisiblePieces());
        playAreaGraphics.repaint();
        frame.setTitle("Cleared rows: " + (playArea.getClearedRows()) + " Timer delay: " + timer.getDelay());
    }
}
