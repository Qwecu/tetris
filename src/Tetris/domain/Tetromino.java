/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.domain;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Varpunen
 */
public abstract class Tetromino {

    protected ArrayList<Square> allPieces;
    protected int areaWidth;
    protected int areaHeight;
    protected int x; //coordinates of the top left corner of an invisible square containing this
    protected int y;
    protected int pieceWidth;
    protected Color color;
    protected ArrayList<Square> containedPieces;
    private static Random r = new Random();

    public static Tetromino newTetromino(int width, int height, Color color, ArrayList<Square> allPieces) {
        int random = r.nextInt(7);
        if (random == 0) {
            return new TetrominoI(width, height, color, allPieces);
        }
        if (random == 1) {
            return new TetrominoO(width, height, color, allPieces);
        }
        if (random == 2) {
            return new TetrominoT(width, height, color, allPieces);
        }
        if (random == 3) {
            return new TetrominoS(width, height, color, allPieces);
        }
        if (random == 4) {
            return new TetrominoZ(width, height, color, allPieces);
        }
        if (random == 5) {
            return new TetrominoJ(width, height, color, allPieces);
        }
        if (random == 6) {
            return new TetrominoL(width, height, color, allPieces);
        }
        return null;
    }

    protected Tetromino(int width, int height, Color color, ArrayList<Square> allPieces) {
        
        areaWidth = width;
        areaHeight = height;
        
        x = areaWidth / 2 - 2;
        y = 0;
        
        this.color = color;
        this.allPieces = allPieces;
        containedPieces = new ArrayList<>();
        createPieces();

        
    }

    protected abstract void createPieces();

    public boolean dropOne() {
        boolean ok = hasSpaceToMoveOne(0, 1);
        if (ok) {
            for (Square containedPiece : containedPieces) {
                containedPiece.dropOne();
            }
            y++;
            return true;
        }
        return false;

    }

    public ArrayList<Square> getPieces() {
        return containedPieces;
    }

    public boolean hasSpaceToMoveOne(int dx, int dy) {
        ArrayList<Square> potential = new ArrayList();
        for (Square piece : containedPieces) {
            potential.add(new Square(areaWidth, areaHeight, piece.getX() + dx, piece.getY() + dy, color, allPieces));
        }
        for (Square square : potential) {
            if (square.getY() >= areaHeight || square.getX() >= areaWidth || square.getX() < 0 || square.getY() < 0) {
                return false;
            }
        }
        for (Square square : potential) {
            for (Square other : allPieces) {
                if (square.collides(other)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* public boolean spaceIsVacant(int x, int y) {
        if (y >= areaHeight || x >= areaWidth || x < 0 || y < 0) {
            return false;
        }
        for (Square square : allPieces) {
            if (square.getX() == x && square.getY() == y) {
                return false;
            }
        }
        return true;
    }*/
    public boolean collides(Square other) {
        for (Square a : containedPieces) {
            if (a.collides(other)) {
                return true;
            }
        }
        return false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return color;
    }

    public void dropDown() {
        while (hasSpaceToMoveOne(0, 1)) {
            dropOne();
        }
    }

    public void moveLeft() {
        if (hasSpaceToMoveOne(-1, 0)) {
            for (Square containedPiece : containedPieces) {
                containedPiece.moveLeft();
            }
            x--;
        }

    }

    public void moveRight() {
        if (hasSpaceToMoveOne(1, 0)) {
            for (Square containedPiece : containedPieces) {
                containedPiece.moveRight();
            }
            x++;
        }

    }

    public void turn() {
        ArrayList<Square> potential = thisRotated();
        for (Square ga : potential) {
            if (ga.getY() >= areaHeight || ga.getX() >= areaWidth || ga.getX() < 0 || ga.getY() < 0) {
                System.out.println("Cannot turn tetramino, would go outside play area");
                return;
            }
        }
        for (Square ga : potential) {
            for (Square other : allPieces) {
                if (ga.collides(other)) {
                    System.out.println("Cannot turn tetramino, would collide other pieces");
                    return;
                }
            }
        }
        containedPieces = potential;
    }

    private ArrayList<Square> thisRotated() {
        ArrayList<Square> potential = new ArrayList<>();
        int[] xCoordinates = new int[pieceWidth];
        int[] yCoordinates = new int[pieceWidth];
        for (int i = 0; i < pieceWidth; i++) {
            xCoordinates[i] = x + i;
            yCoordinates[i] = y + i;
        }
        System.out.println(Arrays.toString(xCoordinates));
        System.out.println(Arrays.toString(yCoordinates));
        System.out.println("x: " + x + " y: " + y);
        for (Square containedPiece : containedPieces) {
            int oldx = containedPiece.getX();
            int oldy = containedPiece.getY();
            int newx = 0;
            int newy = 0;
            for (int i = 0; i < yCoordinates.length; i++) {
                if (yCoordinates[i] == oldy) {
                    newx = x + (pieceWidth - 1) - i;
                }
                if (xCoordinates[i] == oldx) {
                    newy = y + i;
                }
            }

            System.out.println("Old: " + oldx + " " + oldy);
            Square newSquare = new Square(areaWidth, areaHeight, newx, newy, color, allPieces);
            System.out.println("New: " + newSquare.getX() + " " + newSquare.getY());
            potential.add(newSquare);
        }
        return potential;
    }

}
