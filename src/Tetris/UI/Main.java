/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.UI;

import javax.swing.SwingUtilities;

/**
 *
 * @author Varpunen
 */
public class Main {
    public static void main(String[] args) {
        Tetris tetris = new Tetris();
        SwingUtilities.invokeLater(tetris);
    }
}