/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.domain;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Varpunen
 */
public class TetrominoT extends Tetromino {

    public TetrominoT(int width, int height, Color color, ArrayList<Square> allPieces) {
    super(width, height, color, allPieces);
        pieceWidth = 3;
    }

    @Override
    protected void createPieces() {
        for (int i = 0; i < 3; i++) {
            containedPieces.add(new Square(areaWidth, areaHeight, areaWidth / 2 - 1, i, this.color, allPieces));
        }
        containedPieces.add(new Square(areaWidth, areaHeight, areaWidth / 2, 1, this.color, allPieces));
    }
    
}
