/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.domain;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author Varpunen
 */
public class TetrominoI extends Tetromino {
    

    public TetrominoI(int width, int height, Color color, ArrayList<Square> allPieces) {       
        super(width, height, color, allPieces);
        pieceWidth = 4;
    }

    @Override
    protected void createPieces() {
        for (int i = 0; i < 4; i++) {
            containedPieces.add(new Square(areaWidth, areaHeight, areaWidth / 2, i, this.color, allPieces));
        }
    }
}
