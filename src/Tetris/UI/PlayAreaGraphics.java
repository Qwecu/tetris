/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tetris.UI;

import Tetris.domain.*;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Varpunen
 */
public class PlayAreaGraphics extends JPanel{
    
    //List<GravityAffected> pieces;
    //List<GravityAffected> movingPieces;
    List<Square> visiblePieces;
    private int pixelsWidth;
    private int pixelsHeight;
    private int width;
    private int height;
    
    public PlayAreaGraphics(int screenW, int screenH, int areaW, int areaH){
        visiblePieces = new ArrayList<Square>();
        pixelsWidth = screenW;
        pixelsHeight = screenH;
        width = areaW;
        height = areaH;     
    }
    
    public void setPieces(ArrayList<Square> list){
        visiblePieces = list;
    }
    
    public void paintComponent(Graphics g) { 
        super.paintComponent(g);
        g.setColor(Color.lightGray);
        g.fillRect(0, 0, pixelsWidth, pixelsHeight);
       for (Square piece : visiblePieces) {
            g.setColor(piece.getColor());
            g.fill3DRect(pixelsWidth/width*piece.getX(), pixelsHeight/height*piece.getY(), pixelsWidth/width, pixelsHeight/height, true);
        } 
    }
}
